import Vuex from 'vuex'
import axios from 'axios'

export const state = {
    tasks: []
}

export const mutations = {
    setTasks(state, tasks){
        state.tasks = tasks
    },
    addTask(state, task){
        var existed = false;
        for (let key in state.tasks){
            if (task.id === state.tasks[key].id){
                state.tasks[key] = task;
                existed = true;
                break;
            }
        }
        if (!existed){
            state.tasks.push(task);
        }
    },
    removeTask(state, task){
        var tasks = [];
        for (let key in state.tasks){
            if (task.id != state.tasks[key].id){
                tasks.push(state.tasks[key]);
            }
        }
        state.tasks = tasks;
    }
    
}

export const getters = {
    loadTasks(state){
        return state.tasks
    }
}

export const actions = {
    nuxtServerInit({commit}, {req}){
        return axios.get(process.env.firebaseUrl + "/tasks.json")
                    .then(res => {
                        var tasks =  [];
                        for (let key in res.data){
                            tasks.push({id: key, ...res.data[key]});
                        }
                        commit('setTasks', tasks);
                    })
                    .catch(err => console.log(err));
    }
}